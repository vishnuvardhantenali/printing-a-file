﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrintFileUsingDialog
{
    public partial class Form1 : Form
    {
        private int indexValue = 0;
        private static string[] printIndexFiles = null;
        private PrintDialog printDialog = new PrintDialog();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (printIndexFiles == null || printIndexFiles.Count() <= 0) 
            {
                MessageBox.Show("Please select the Files");
                return;
            }
            DialogResult result = printDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                // Create a PrintDocument instance
                PrintDocument printDocument = new PrintDocument();

                // Set the PrintDocument to use the selected printer from the PrintDialog
                printDocument.PrinterSettings = printDialog.PrinterSettings;

                // Hook up the PrintPage event handler to provide content to print
                printDocument.PrintPage += PrintDocument_PrintPage;

                // Start the printing process
                printDocument.Print();
            }
        }
        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            string pageIndex = printIndexFiles[indexValue];
            using (Image imageStream = Image.FromFile(pageIndex))
            {
                e.Graphics.DrawImage(imageStream, 35, 35, e.MarginBounds.Width + e.MarginBounds.X, e.MarginBounds.Height + e.MarginBounds.Y);
            }
            indexValue++;

            // If there are more pages, set HasMorePages to true; otherwise, set it to false to end the printing process
            e.HasMorePages = indexValue < printIndexFiles.Length;
        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            // Enable multiple file selection
            openFileDialog.Multiselect = true;
            
            // Enable multiple file selection
            openFileDialog.RestoreDirectory=true;

            // Set up the filter to display only image files
            openFileDialog.Filter = "Image Files (*.jpg; *.jpeg; *.png; *.gif; *.bmp)|*.jpg; *.jpeg; *.png; *.gif; *.bmp|All Files (*.*)|*.*";
            
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                // Access the selected file paths
                string[] selectedFiles = openFileDialog.FileNames;
                printIndexFiles = selectedFiles;

                // Do something with the selected files
                foreach (string filePath in selectedFiles)
                {
                    // Process each filePath here
                    // Append the current file path to the TextBox
                    txtFiles.Text += Path.GetFileName(filePath) + Environment.NewLine;
                }
            }
        }
    }
}
