﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace PrintFileUsingPrintDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        int indexValue { get; set; } = 0;
        string[] printIndexFiles = new string[]
        {
            @"C:\Users\Admin\AppData\Local\Sharp\Sharpdesk\.sdesktop\.4061124323_pdf 11 pages.pdf\.image\P_0001.png",
            @"C:\Users\Admin\AppData\Local\Sharp\Sharpdesk\.sdesktop\.4061124323_pdf 11 pages.pdf\.image\P_0002.png"
        };
        private void PrintFiles()
        {
            PrintDialog printDialog = new PrintDialog();

            if (printDialog.ShowDialog() == true)
            {
                // Perform printing here
                PrintQueue selectedPrinter = printDialog.PrintQueue;
                // You can use the selected printer and its settings for printing.
            }

        }
    }
}
